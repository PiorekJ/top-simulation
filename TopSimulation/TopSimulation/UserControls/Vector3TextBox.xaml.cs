﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OpenTK;

namespace TopSimulation.UserControls
{
    /// <summary>
    /// Interaction logic for Vector3TextBox.xaml
    /// </summary>
    public partial class Vector3TextBox : UserControl, INotifyPropertyChanged
    {
        public Vector3 Value
        {
            get
            {
                return (Vector3)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public IValueConverter CustomConverter { get; set; }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(nameof(Value), typeof(Vector3), typeof(Vector3TextBox), new PropertyMetadata(Vector3.Zero, OnValueChanged));

        public float X
        {
            get { return (float)Math.Round(((Vector3)GetValue(ValueProperty)).X, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector3(value, Value.Y, Value.Z);
                OnPropertyChanged();
            }
        }

        public float Y
        {
            get { return (float)Math.Round(((Vector3)GetValue(ValueProperty)).Y, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector3(Value.X, value, Value.Z);
                OnPropertyChanged();
            }
        }

        public float Z
        {
            get { return (float)Math.Round(((Vector3)GetValue(ValueProperty)).Z, 3, MidpointRounding.AwayFromZero); }
            set
            {
                Value = new Vector3(Value.X, Value.Y, value);
                OnPropertyChanged();
            }
        }

        public Vector3TextBox()
        {
            InitializeComponent();
            ((FrameworkElement)Content).DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }

        private static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Vector3TextBox control = sender as Vector3TextBox;
            if (control != null)
            {
                if (control.PropertyChanged == null)
                    return;

                control.PropertyChanged(sender, new PropertyChangedEventArgs("X"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Y"));
                control.PropertyChanged(sender, new PropertyChangedEventArgs("Z"));
            }
        }
    }
}
