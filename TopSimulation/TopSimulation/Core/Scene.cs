﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;
using OpenTK;
using TopSimulation.Models;
using TopSimulation.Utils;

namespace TopSimulation.Core
{
    public class Scene : BindableObject
    {
        public Camera SceneCamera;
        public LightModel SceneLight;

        public TopModel TopModel
        {
            get { return _topModel; }
            set
            {
                _topModel = value; 
                RaisePropertyChanged();
            }
        }

        public FloorGridModel FloorGridModel
        {
            get { return _floorGridModel; }
            set
            {
                _floorGridModel = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Model> SceneModels;
        private TopModel _topModel;
        private FloorGridModel _floorGridModel;
        public Scene()
        {
            SceneCamera = new Camera(this);
            SceneModels = new ObservableCollection<Model>();
        }

        public PointModel AddPoint()
        {
            PointModel pointModel = new PointModel(this);
            SceneModels.Add(pointModel);
            return pointModel;
        }

        public PointModel AddPoint(Vector3 pos)
        {
            PointModel pointModel = new PointModel(this, pos);
            SceneModels.Add(pointModel);
            return pointModel;
        }

        public TopModel AddTopModel()
        {
            TopModel model = new TopModel(this);
            TopModel = model;
            SceneModels.Add(model);
            return model;
        }

        public LightModel AddLightModel()
        {
            LightModel model = new LightModel(this);
            SceneLight = model;
            SceneModels.Add(model);
            return model;
        }

        public SceneCursor AddSceneCursor()
        {
            SceneCursor model = new SceneCursor(this);
            SceneModels.Add(model);
            return model;
        }

        public FloorGridModel AddFloorGridModel()
        {
            FloorGridModel model = new FloorGridModel(this);
            SceneModels.Add(model);
            return model;
        }

        public void AddModel(Model model)
        {
            SceneModels.Add(model);
        }

        public void RemoveModel(Model model)
        {
            SceneModels.Remove(model);
        }
    }
}