﻿using TopSimulation.OpenTK;

namespace TopSimulation.Core
{
    public static class Shaders
    {
        private static Shader _basicShader;
        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _textureShader;
        public static Shader TextureShader
        {
            get
            {
                if (_textureShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Texture/TextureShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Texture/TextureShader.frag"))
                    {
                        _textureShader = new Shader(vert, frag);
                    }
                    return _textureShader;
                }
                return _textureShader;
            }
        }

        private static Shader _lightShader;
        public static Shader LightShader
        {
            get
            {
                if (_lightShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Light/LightShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Light/LightShader.frag"))
                    {
                        _lightShader = new Shader(vert, frag);
                    }
                    return _lightShader;
                }
                return _lightShader;
            }
        }

        private static Shader _litObjectShader;
        public static Shader LitObjectShader
        {
            get
            {
                if (_litObjectShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Lightning/Object/LightObjectShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Lightning/Object/LightObjectShader.frag"))
                    {
                        _litObjectShader = new Shader(vert, frag);
                    }
                    return _litObjectShader;
                }
                return _litObjectShader;
            }
        }

        private static Shader _polyChainShader;
        public static Shader PolyChainShader
        {
            get
            {
                if (_polyChainShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PolyChain/PolyShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/PolyChain/PolyShader.frag"),
                        geom = new GeometricShaderObject("./Shaders/PolyChain/PolyShader.geom"))
                    {
                        _polyChainShader = new Shader(vert, frag, geom);
                    }
                    return _polyChainShader;
                }
                return _polyChainShader;
            }
        }

        private static Shader _normalShader;
        public static Shader NormalShader
        {
            get
            {
                if (_normalShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Normal/NormalShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Normal/NormalShader.frag"),
                        geom = new GeometricShaderObject("./Shaders/Normal/NormalShader.geom"))
                    {
                        _normalShader = new Shader(vert, frag, geom);
                    }
                    return _normalShader;
                }
                return _normalShader;
            }
        }
    }
}
