﻿using TopSimulation.OpenTK;

namespace TopSimulation.Core
{
    public class Textures
    {
        private static Texture _woodTexture;
        public static Texture WoodTexture
        {
            get
            {
                if (_woodTexture == null)
                {
                    _woodTexture = new Texture("./Textures/WoodTex.jpg");
                    return _woodTexture;
                }
                return _woodTexture;
            }
        }
    }
}
