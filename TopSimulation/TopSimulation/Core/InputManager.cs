﻿using System;
using System.Windows.Input;

namespace TopSimulation.Core
{
    public static class InputManager
    {
        private static bool[] _keysState = new bool[Enum.GetNames(typeof(Key)).Length];
        private static bool[] _mouseState = new bool[Enum.GetNames(typeof(MouseButton)).Length];

        public delegate void MouseScrollEventHandler(int delta);
        public static event MouseScrollEventHandler OnMouseScrollEvent;

        public static void OnKeyChange(Key key, bool state)
        {
            _keysState[(int)key] = state;
        }

        public static void OnMouseButtonChange(MouseButton button, bool state)
        {
            _mouseState[(int)button] = state;
        }

        public static void OnMouseScroll(int delta)
        {
            OnMouseScrollEvent?.Invoke(delta);
        }

        public static bool IsKeyDown(Key key)
        {
            return _keysState[(int)key];
        }

        public static bool IsMouseButtonDown(MouseButton key)
        {
            return _mouseState[(int)key];
        }
    }
}
