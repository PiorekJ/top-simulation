﻿using System;
using System.Windows.Media.Imaging;
using OpenTK;
using TopSimulation.Models;
using TopSimulation.Utils;

namespace TopSimulation.Core
{
    public class Simulation : BindableObject
    {
        public static Vector3 GravityConstant = new Vector3(0, -9.81f, 0);

        public Scene Scene
        {
            get { return _scene; }
            set
            {
                _scene = value;
                RaisePropertyChanged();
            }
        }

        public bool IsGravityOn
        {
            get { return _isGravityOn; }
            set
            {
                _isGravityOn = value;
                RaisePropertyChanged();
            }
        }

        public TimeCounter Time;
        private TopModel _model;

        public static float SimulationDT = 1 / 100f;
        private Vector3 GravitationForce = GravityConstant;
        private Scene _scene;
        private bool _isGravityOn = true;

        public static float DeltaTime => TimeCounter.DeltaTime;

        public bool IsRunning { get; set; }

        public Simulation()
        {
            Scene = new Scene();
            Time = new TimeCounter();
        }

        public void SetSimulationValues()
        {
            _model.FreeEndPosition = new Vector3(_model.CubeSide, _model.CubeSide, _model.CubeSide);
            _model.InertiaTensor = MathExt.GetCubeInertiaTensor(_model.CubeSide, _model.CubeDensity);
            _model.InertiaTensorInverse = Matrix3.Invert(_model.InertiaTensor);
            _model.CubeCenter = new Vector3(_model.CubeSide / 2, _model.CubeSide / 2, _model.CubeSide / 2);
            _model.CubeMass = _model.CubeDensity * _model.CubeSide * _model.CubeSide * _model.CubeSide;
            GravitationForce = GravityConstant * _model.CubeMass;
        }

        public void Reset()
        {
            _model.ShowTrace = false;
            IsGravityOn = true;
            Scene.FloorGridModel.IsDisplayed = true;
            _model.ResetCube();
        }

        public void InitializeSimulation()
        {
            Time.Start();
            Scene.AddLightModel();
            Scene.AddSceneCursor();
            _model = Scene.AddTopModel();
            Scene.FloorGridModel = Scene.AddFloorGridModel();
        }



        public void SimulateTop()
        {
            var simHalf = SimulationDT * 0.5f;
            var N = Vector3.Zero;
            if (IsGravityOn)
            {
                var gravity = _model.Rotation.Inverted() * GravitationForce;
                N = Vector3.Cross(_model.CubeCenter, gravity);
            }
            var wk1 = CalculateAngularVelocity(_model.AngularVelocity, N);
            var wk2 = CalculateAngularVelocity(_model.AngularVelocity + simHalf * wk1, N);
            var wk3 = CalculateAngularVelocity(_model.AngularVelocity + simHalf * wk2, N);
            var wk4 = CalculateAngularVelocity(_model.AngularVelocity + SimulationDT * wk3, N);

            var newAngularVelocity = _model.AngularVelocity + SimulationDT / 6.0f * (wk1 + 2.0f * wk2 + 2.0f * wk3 + wk4);

            var qk1 = CalculateQuaternion(_model.Rotation, newAngularVelocity);
            var qk2 = CalculateQuaternion(_model.Rotation + simHalf * qk1, newAngularVelocity);
            var qk3 = CalculateQuaternion(_model.Rotation + simHalf * qk2, newAngularVelocity);
            var qk4 = CalculateQuaternion(_model.Rotation + SimulationDT * qk3, newAngularVelocity);

            var newQuaternion = _model.Rotation + SimulationDT / 6.0f * (qk1 + 2 * qk2 + 2 * qk3 + qk4);

            _model.AngularVelocity = newAngularVelocity;
            _model.Rotation = newQuaternion.Normalized();
        }

        public Vector3 CalculateAngularVelocity(Vector3 velocity, Vector3 torque)
        {
            return _model.InertiaTensorInverse * (torque + Vector3.Cross(_model.InertiaTensor * velocity, velocity));
        }

        public Quaternion CalculateQuaternion(Quaternion initialRotation, Vector3 velocity)
        {
            return initialRotation.Normalized() * new Quaternion(velocity * 0.5f, 0);
        }
    }
}
