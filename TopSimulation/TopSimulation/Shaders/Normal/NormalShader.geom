﻿#version 330 core

layout(lines) in;
layout(line_strip, max_vertices = 4) out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 normalColor;

mat4 projectionViewModel = projection * view * model;
float normal_length = 0.5f;

in V_Out
{
	vec3 color;
	vec3 normal;
} In[];

out GEO_Out
{
	vec3 color;
} Out;

void DrawLine(vec4 a, vec4 b, vec3 color)
{
	Out.color = color;
	gl_Position = a;
	EmitVertex();
	gl_Position = b;
	EmitVertex();

	EndPrimitive();
}

void main()
{
		vec3 P1 = gl_in[0].gl_Position.xyz;
		vec3 P2 = gl_in[1].gl_Position.xyz;
		vec3 N = In[0].normal;
    
		vec4 point1 = projectionViewModel * vec4(P1, 1.0);
		vec4 point2 = projectionViewModel * vec4(P2, 1.0);
		vec4 norm = projectionViewModel * vec4(P1 + N * normal_length, 1.0);
		
		DrawLine(point1,norm,normalColor);		
		//	DrawLine(point1, point2, In[1].color);
	
}