﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 normal;
in layout(location = 2) vec3 color;

out V_Out
{
	vec3 color;
	vec3 normal;
} Out;

void main()
{
	gl_Position = vec4(position,1);
	Out.color = color;
	Out.normal = normal;
}

