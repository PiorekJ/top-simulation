﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using TopSimulation.Core;
using TopSimulation.Models;
using OpenTK.Graphics.OpenGL;
using InputManager = TopSimulation.Core.InputManager;

namespace TopSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetCurrentAspectRatio(DisplayControl.AspectRatio);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);

            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
            GL.LineWidth(1);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            Simulation.InitializeSimulation();

            CompositionTarget.Rendering += OnRender;
        }

        private void OnRender(object sender, EventArgs e)
        {
            Simulation.Time.CountDT();
            GL.ClearColor(System.Drawing.Color.Black);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Simulation.Scene.SceneCamera.UpdateCamera(DisplayControl.MousePosition);

            for (var i = 0; i < Simulation.Scene.SceneModels.Count; i++)
            {
                Model model = Simulation.Scene.SceneModels[i];
                model.OnUpdate();
                model.OnRender();
            }

            if(Simulation.IsRunning)
                Simulation.SimulateTop();

            DisplayControl.SwapBuffers();
        }

        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetCurrentAspectRatio(DisplayControl.AspectRatio);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeigth);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            foreach (Model model in Simulation.Scene.SceneModels)
            {
                model.Dispose();
            }
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }

        private void Play_OnClick(object sender, RoutedEventArgs e)
        {
            Simulation.SetSimulationValues();
            Simulation.IsRunning = true;
        }

        private void Stop_OnClick(object sender, RoutedEventArgs e)
        {
            Simulation.IsRunning = false;
        }

        private void Reset_OnClick(object sender, RoutedEventArgs e)
        {
            Simulation.IsRunning = false;
            Simulation.Reset();
        }
    }
}
