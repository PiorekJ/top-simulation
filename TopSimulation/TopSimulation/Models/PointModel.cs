﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using TopSimulation.Core;
using TopSimulation.OpenTK;
using TopSimulation.Utils;
using OpenTK;

namespace TopSimulation.Models
{
    public class PointModel : Model, IMovable, IDisposable, ISelectable, IColorable
    {
        private Color _color = Colors.White;
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                RaisePropertyChanged();
            }
        }

        public PointModel(Scene currScene) : base(currScene)
        {
            Shader = Shaders.BasicShader;
            Mesh = GeneratePointMesh();
        }

        public PointModel(Scene currScene, Vector3 position) : base(currScene)
        {
            Shader = Shaders.BasicShader;
            Mesh = GeneratePointMesh();
            Position = position;
        }

        public override void SetMesh()
        {
            Mesh = GeneratePointMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        public void Translate(Vector3 newPosition)
        {
            throw new NotImplementedException();
        }

        public void Rotate(Vector3 newRotation)
        {
            throw new NotImplementedException();
        }

        void IMovable.Scale(Vector3 newScale)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Mesh?.Dispose();
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void Deselect()
        {
            throw new NotImplementedException();
        }

        private IMesh GeneratePointMesh()
        {
            Mesh?.Dispose();
            return new Mesh<VertexPC>(new List<VertexPC>() { new VertexPC(Position, Color.ColorToVector3()) }, null, MeshType.Points, AccessType.Static);
        }

        public override void OnUpdate()
        {
        }

        public override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Mesh.Draw();
        }
    }
}
