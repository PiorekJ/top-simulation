﻿using System.Collections.Generic;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TopSimulation.Core;
using TopSimulation.OpenTK;
using TopSimulation.Utils;

namespace TopSimulation.Models
{
    public class FloorGridModel : Model, IColorable
    {
        public Color Color { get; set; } = Colors.White;

        public bool IsDisplayed
        {
            get { return _isDisplayed; }
            set
            {
                _isDisplayed = value; 
                RaisePropertyChanged();
            }
        }

        public int SizeX { get; set; } = 10;
        public int SizeZ { get; set; } = 10;

        public int GridDivisionsX { get; set; } = 2;
        public int GridDivisionsZ { get; set; } = 2;

        private Mesh<VertexPNC> NormalMesh;
        private Shader NormalShader;
        private bool _isDisplayed = true;

        public FloorGridModel(Scene currScene) : base(currScene)
        {
            Shader = Shaders.LitObjectShader;
            Mesh = GenerateLitFloorMesh();
            NormalMesh = GenerateFloorGridMesh();
            NormalShader = Shaders.NormalShader;
        }

        public override void SetMesh()
        {
            Mesh = GenerateLitFloorMesh();
            NormalMesh = GenerateFloorGridMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.LitObjectShader;
            NormalShader = Shaders.NormalShader;
        }

        public override void OnRender()
        {
            if (IsDisplayed)
            {
                
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                Shader.Bind(Shader.GetUniformLocation("alpha"), 0.5f);
                Shader.Bind(Shader.GetUniformLocation("lightColor"), Scene.SceneLight.Color.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("lightPos"), Scene.SceneLight.Position);
                Shader.Bind(Shader.GetUniformLocation("viewPos"), Scene.SceneCamera.Position);
                Mesh.Draw();
                
                NormalShader.Use();
                NormalShader.Bind(NormalShader.GetUniformLocation("model"), GetModelMatrix());
                NormalShader.Bind(NormalShader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                NormalShader.Bind(NormalShader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                NormalShader.Bind(NormalShader.GetUniformLocation("normalColor"), Colors.Yellow.ColorToVector3());
                NormalMesh.Draw();
            }
        }

        public Mesh<VertexPNC> GenerateLitFloorMesh()
        {
            List<VertexPNC> vertices = new List<VertexPNC>();
            List<uint> edges = new List<uint>();

            float distX = SizeX / (float)(GridDivisionsX - 1);
            float distZ = SizeZ / (float)(GridDivisionsZ - 1);

            float startX = (-SizeX / 2.0f);
            float startZ = (-SizeZ / 2.0f);

            for (int i = 0; i < GridDivisionsZ; i++)
            {
                for (int j = 0; j < GridDivisionsX; j++)
                {
                    vertices.Add(new VertexPNC(new Vector3(startX + j * distX, 0, startZ + i * distZ), -Simulation.GravityConstant.Normalized(), Color.ColorToVector3()));
                }
            }

            edges.Add(0);
            edges.Add(1);
            edges.Add(2);

            edges.Add(1);
            edges.Add(3);
            edges.Add(2);
            return new Mesh<VertexPNC>(vertices, edges, MeshType.Triangles, AccessType.Static);
        }

        public Mesh<VertexPNC> GenerateFloorGridMesh()
        {
            List<VertexPNC> vertices = new List<VertexPNC>();
            List<uint> edges = new List<uint>();

            float distX = SizeX / (float)(GridDivisionsX - 1);
            float distZ = SizeZ / (float)(GridDivisionsZ - 1);

            float startX = (-SizeX / 2.0f);
            float startZ = (-SizeZ / 2.0f);

            for (int i = 0; i < GridDivisionsZ; i++)
            {
                for (int j = 0; j < GridDivisionsX; j++)
                {
                    vertices.Add(new VertexPNC(new Vector3(startX + j * distX, 0, startZ + i * distZ), Simulation.GravityConstant.Normalized(), Color.ColorToVector3()));

                    int currentIndex = GridDivisionsX * j + i;
                    int topNeighbour = GridDivisionsX * j + (i + 1) % GridDivisionsX;
                    int rightNeighbour = GridDivisionsZ * ((j + 1) % GridDivisionsX) + i;
                    edges.Add((uint)currentIndex);
                    edges.Add((uint)topNeighbour);
                    edges.Add((uint)currentIndex);
                    edges.Add((uint)rightNeighbour);
                }
            }

            return new Mesh<VertexPNC>(vertices, edges, MeshType.Lines, AccessType.Static);
        }


    }
}
