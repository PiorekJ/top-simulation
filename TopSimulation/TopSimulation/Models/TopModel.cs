﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using TopSimulation.Core;
using TopSimulation.OpenTK;
using OpenTK;
using TopSimulation.Utils;

namespace TopSimulation.Models
{
    public class TopModel : Model
    {
        public Vector3 AngularVelocity
        {
            get { return _angularVelocity; }
            set
            {
                _angularVelocity = value;
                RaisePropertyChanged();
                DisplayAngularVelocity = _angularVelocity.ToString();
            }
        }

        public Matrix3 InertiaTensor;
        public Matrix3 InertiaTensorInverse;
        public Vector3 FreeEndPosition;

        public int MaxTracePoints
        {
            get { return _maxTracePoints; }
            set
            {
                _maxTracePoints = value; 
                RaisePropertyChanged();
            }
        }

        public Color Color { get; set; } = Colors.LawnGreen;

        public float DiagAngularVelocity
        {
            get { return _diagAngularVelocity; }
            set
            {
                _diagAngularVelocity = value;
                AngularVelocity = new Vector3(1.0f).Normalized() * _diagAngularVelocity;
                RaisePropertyChanged();
            }
        }

        public float InitialAngle
        {
            get { return _initialAngle; }
            set
            {
                _initialAngle = value;
                Rotation = BasicRotation * Quaternion.FromAxisAngle(Vector3.UnitZ, MathExt.Deg2Rad(value));
                RaisePropertyChanged();
            }
        }

        public string DisplayAngularVelocity
        {
            get { return _displayAngularVelocity; }
            set
            {
                _displayAngularVelocity = value;
                RaisePropertyChanged();
            }
        }

        private Vector3 _cubeCenter;
        public Vector3 CubeCenter
        {
            get { return _cubeCenter; }
            set
            {
                _cubeCenter = value;
                RaisePropertyChanged();
            }
        }

        private float _cubeSide = 1;
        public float CubeSide
        {
            get { return _cubeSide; }
            set
            {
                _cubeSide = value;
                LitObjectMesh = GenerateTriangleCornerMesh(CubeSide);
                WireframeMesh = GenerateWireFrameCornerMesh(CubeSide);
                if (_showWireFrame)
                    Mesh = WireframeMesh;
                else
                    Mesh = LitObjectMesh;
                RaisePropertyChanged();
            }
        }

        private float _cubeDensity = 1;
        public float CubeDensity
        {
            get { return _cubeDensity; }
            set
            {
                _cubeDensity = value;
                RaisePropertyChanged();
            }
        }

        private float _cubeMass = 1;
        public float CubeMass
        {
            get { return _cubeMass; }
            set
            {
                _cubeMass = value;
                RaisePropertyChanged();
            }
        }

        private Color DiagonalColor = Colors.Red;

        private readonly Quaternion BasicRotation =
            Quaternion.FromAxisAngle(Vector3.UnitX, -(float)Math.Atan(1 / Math.Sqrt(2))) *
            Quaternion.FromAxisAngle(Vector3.UnitZ, MathExt.Deg2Rad(45));

        private Mesh<VertexPNC> LitObjectMesh;
        private Mesh<VertexPC> WireframeMesh;
        private string _displayAngularVelocity;

        private bool _showWireFrame;
        public bool ShowWireFrame
        {
            get { return _showWireFrame; }
            set
            {
                _showWireFrame = value;
                if (_showWireFrame)
                {
                    Mesh = WireframeMesh;
                    Shader = Shaders.BasicShader;
                }
                else
                {
                    Mesh = LitObjectMesh;
                    Shader = Shaders.LitObjectShader;
                }
                RaisePropertyChanged();
            }
        }

        private bool _showTrace;
        private float _currTraceDelay = 0.0f;
        public TraceLine TraceLineModel { get; set; }
        private float _initialAngle;
        private Vector3 _angularVelocity;
        private float _diagAngularVelocity;
        private int _maxTracePoints = 1000;


        public bool ShowTrace
        {
            get { return _showTrace; }
            set
            {
                _showTrace = value;

                if (_showTrace)
                {
                    if (TraceLineModel == null)
                        TraceLineModel = new TopModel.TraceLine(Scene, Rotation * FreeEndPosition, MaxTracePoints);
                    TraceLineModel.ResetPoints(Rotation * FreeEndPosition);
                }
                
                RaisePropertyChanged();
            }
        }

        public TopModel(Scene currScene) : base(currScene)
        {
            LitObjectMesh = GenerateTriangleCornerMesh(CubeSide);
            WireframeMesh = GenerateWireFrameCornerMesh(CubeSide);
            Mesh = LitObjectMesh;
            Shader = Shaders.LitObjectShader;
            Rotation = BasicRotation;
            FreeEndPosition = new Vector3(CubeSide, CubeSide, CubeSide);

        }

        public void ResetCube()
        {
            Rotation = BasicRotation * Quaternion.FromAxisAngle(Vector3.UnitZ, MathExt.Deg2Rad(InitialAngle));
            AngularVelocity = new Vector3(1.0f).Normalized() * DiagAngularVelocity;
            TraceLineModel.ResetPoints(Rotation * FreeEndPosition);
        }

        public override void OnUpdate()
        {
        }

        public override void OnRender()
        {
            if (ShowTrace)
            {
                TraceLineModel.DrawTraceLine(Rotation * FreeEndPosition);
            }

            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("alpha"), 1.0f);
            if (!_showWireFrame)
            {
                Shader.Bind(Shader.GetUniformLocation("lightColor"), Scene.SceneLight.Color.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("lightPos"), Scene.SceneLight.Position);
                Shader.Bind(Shader.GetUniformLocation("viewPos"), Scene.SceneCamera.Position);
            }

            Mesh.Draw();
        }


        public override void SetMesh()
        {
            Mesh = GenerateWireFrameMesh(CubeSide);
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        #region MeshGeneration

        private Mesh<VertexPC> GenerateWireFrameMesh(float sideSize)
        {
            List<VertexPC> vertices = new List<VertexPC>()
            {
                new VertexPC(-sideSize, -sideSize, sideSize, Color.ColorToVector3()),
                new VertexPC(sideSize, -sideSize, sideSize, Color.ColorToVector3()),
                new VertexPC(sideSize, sideSize, sideSize, Color.ColorToVector3()),
                new VertexPC(-sideSize, sideSize, sideSize, Color.ColorToVector3()),
                new VertexPC(-sideSize, -sideSize, -sideSize, Color.ColorToVector3()),
                new VertexPC(sideSize, -sideSize, -sideSize, Color.ColorToVector3()),
                new VertexPC(sideSize, sideSize, -sideSize, Color.ColorToVector3()),
                new VertexPC(-sideSize, sideSize, -sideSize, Color.ColorToVector3()),
                new VertexPC(-sideSize, -sideSize, sideSize, DiagonalColor.ColorToVector3()),
                new VertexPC(sideSize, sideSize, -sideSize, DiagonalColor.ColorToVector3())
            };

            List<uint> edges = new List<uint>()
            {
                0,1,0,3,2,1,2,3,

                4,5,4,7,6,5,6,7,

                0,4,1,5,2,6,3,7,

                8,9
            };

            return new Mesh<VertexPC>(vertices, edges, MeshType.Lines, AccessType.Static);
        }

        private Mesh<VertexPC> GenerateWireFrameCornerMesh(float sideSize)
        {
            List<VertexPC> vertices = new List<VertexPC>()
            {
                new VertexPC(0f, 0f, sideSize, Color.ColorToVector3()),
                new VertexPC(sideSize, 0f, sideSize, Color.ColorToVector3()),
                new VertexPC(sideSize, sideSize, sideSize, Color.ColorToVector3()),
                new VertexPC(0f, sideSize, sideSize, Color.ColorToVector3()),
                new VertexPC(0f, 0f, 0f, Color.ColorToVector3()),
                new VertexPC(sideSize, 0f, 0f, Color.ColorToVector3()),
                new VertexPC(sideSize, sideSize, 0f, Color.ColorToVector3()),
                new VertexPC(0f, sideSize, 0f, Color.ColorToVector3()),
                new VertexPC(0f, 0f, 0f, DiagonalColor.ColorToVector3()),
                new VertexPC(sideSize, sideSize, sideSize, DiagonalColor.ColorToVector3())
            };

            List<uint> edges = new List<uint>()
            {
                0,1,0,3,2,1,2,3,

                4,5,4,7,6,5,6,7,

                0,4,1,5,2,6,3,7,

                8,9
            };

            return new Mesh<VertexPC>(vertices, edges, MeshType.Lines, AccessType.Static);
        }


        private Mesh<VertexPNC> GenerateTriangleMesh(float sideSize)
        {
            List<VertexPNC> vertices = new List<VertexPNC>()
            {
                new VertexPNC(-sideSize, -sideSize, -sideSize,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize, -sideSize,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize, -sideSize,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize, -sideSize,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize,  sideSize, -sideSize,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize, -sideSize,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),

                new VertexPNC(-sideSize, -sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize,  sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),

                new VertexPNC(-sideSize,  sideSize,  sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize,  sideSize, -sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize, -sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize, -sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize,  sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize,  sideSize,  sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(sideSize,  sideSize, -sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize, -sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize, -sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize,  sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(-sideSize, -sideSize, -sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize, -sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize,  sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, -sideSize,  sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize,  sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize, -sideSize, -sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(-sideSize,  sideSize, -sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize, -sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize,  sideSize,  sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(-sideSize,  sideSize, -sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3())
            };

            return new Mesh<VertexPNC>(vertices, null, MeshType.Triangles, AccessType.Static);
        }

        private Mesh<VertexPNC> GenerateTriangleCornerMesh(float sideSize)
        {
            List<VertexPNC> vertices = new List<VertexPNC>()
            {
                new VertexPNC(0f, 0f, 0f,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f, 0f,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize, 0f,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize, 0f,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(0f,  sideSize, 0f,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f, 0f,  0.0f,  0.0f, -1.0f, Color.ColorToVector3()),

                new VertexPNC(0f, 0f,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(0f,  sideSize,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f,  sideSize,  0.0f,  0.0f,  1.0f, Color.ColorToVector3()),

                new VertexPNC(0f,  sideSize,  sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f,  sideSize, 0f, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f, 0f, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f, 0f, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f,  sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f,  sideSize,  sideSize, -1.0f,  0.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(sideSize,  sideSize, 0f,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f, 0f,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f, 0f,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f,  sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  1.0f,  0.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(0f, 0f, 0f,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f, 0f,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f,  sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize, 0f,  sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f,  sideSize,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f, 0f, 0f,  0.0f, -1.0f,  0.0f, Color.ColorToVector3()),

                new VertexPNC(0f,  sideSize, 0f,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize, 0f,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(sideSize,  sideSize,  sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f,  sideSize, sideSize,  0.0f,  1.0f,  0.0f, Color.ColorToVector3()),
                new VertexPNC(0f,  sideSize, 0f,  0.0f,  1.0f,  0.0f, Color.ColorToVector3())
            };

            return new Mesh<VertexPNC>(vertices, null, MeshType.Triangles, AccessType.Static);
        }

        #endregion

        public class TraceLine : Model, IColorable
        {
            public const float TracePointDelay = 0f;

            public Color Color { get; set; } = Colors.Yellow;
            private float _currTraceDelay = 0.0f;
            private int _currentPoints = 0;

            private int _currentMovePoints = 0;

            private Vector3 _anchorPoint;
            private Vector3 _oldanchorPoint;
            private VertexPC[] _points;
            private int _maxTracePoints;
            public TraceLine(Scene currScene, Vector3 anchor, int maxTracePoints) : base(currScene)
            {
                _oldanchorPoint = anchor;
                _anchorPoint = anchor;
                _maxTracePoints = maxTracePoints;
                _points = new VertexPC[maxTracePoints * 2];
                Mesh = GenerateTraceMesh();
                Shader = Shaders.BasicShader;
            }

            public override void SetMesh()
            {
                Mesh = GenerateTraceMesh();
            }

            public override void SetShader()
            {
                Shader = Shaders.BasicShader;
            }

            public void DrawTraceLine(Vector3 anchor)
            {
                _anchorPoint = anchor;
                OnUpdate();
                OnRender();
                _oldanchorPoint = _anchorPoint;
            }

            public override void OnUpdate()
            {
                if (_currentMovePoints >= _maxTracePoints)
                {
                    _currentMovePoints = 0;
                }
                _points[_currentMovePoints].Position = _oldanchorPoint;
                _currentMovePoints++;
                _points[_currentMovePoints].Position = _anchorPoint;
                _currentMovePoints++;

                ((Mesh<VertexPC>)Mesh).SetVertices(0, _points);
            }

            public override void OnRender()
            {
                Shader.Use();
                Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
                Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
                Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
                Mesh.Draw();
            }

            public void ResetPoints(Vector3 currentAnchor)
            {
                _anchorPoint = currentAnchor;
                _oldanchorPoint = currentAnchor;
                for (int i = 0; i < _maxTracePoints; i++)
                {
                    _points[i].Position = currentAnchor;
                    _points[i].Color = Color.ColorToVector3();
                }
            }

            private Mesh<VertexPC> GenerateTraceMesh()
            {
                ResetPoints(_anchorPoint);

                return new Mesh<VertexPC>(_points, null, MeshType.Lines, AccessType.Stream);
            }
        }
    }
}
