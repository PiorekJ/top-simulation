﻿using System.Windows.Media;
using OpenTK;

namespace TopSimulation.Utils
{
    public static class Extensions
    {
        public static Vector3 ColorToVector3(this Color color)
        {
            return new Vector3(color.ScR, color.ScG, color.ScB);
        }
    }
}
