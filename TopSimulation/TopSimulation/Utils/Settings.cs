﻿using System;
using OpenTK;

namespace TopSimulation.Utils
{
    public static class Settings
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 5, 10);
        public static Vector2 DefaultCameraRotation = new Vector2(MathExt.Deg2Rad(-10), 0);
        public const float DefaultFOV = (float)Math.PI / 4;
        public const float DefaultZNear = 0.01f;
        public const float DefaultZFar = 50f;

        public const float CameraMovementMouseSensitivity = 0.5f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 0.5f;


        public const float CameraMovementKeyVelocity = 5.0f;
        public const float CameraMovementKeySlowVelocity = 0.25f;
    }
}
