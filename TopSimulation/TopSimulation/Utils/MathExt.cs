﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace TopSimulation.Utils
{
    public static class MathExt
    {
        public static float Rad2Deg(float radians)
        {
            return (float)(radians * (180 / Math.PI));
        }

        public static float Deg2Rad(float angle)
        {
            return (float)(angle * (Math.PI / 180));
        }

        public static Matrix3 GetCubeInertiaTensor(float cubeSide, float cubeDensity)
        {
            var cubeSidePow = cubeSide * cubeSide * cubeSide * cubeSide * cubeSide;
            var cubeDiv = -cubeSidePow * cubeDensity / 4.0f;
            var diag = 2.0f / 3.0f * cubeSidePow * cubeDensity;
            var row1 = new Vector3(diag, cubeDiv, cubeDiv);
            var row2 = new Vector3(cubeDiv, diag, cubeDiv);
            var row3 = new Vector3(cubeDiv, cubeDiv, diag);
            return new Matrix3(row1, row2, row3);
        }

    }
}
